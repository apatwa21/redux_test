var express = require("express");
var bunyan = require("bunyan");
const Sequelize = require("sequelize");

const PORT = 8000;
//intialize our express app
var app = express();

//expose router to process all requests before they are routed
var router = express.Router();

//create the bunyan logger
var logger = bunyan.createLogger({
  name: "redux_test"
});

// connect to the database using sequelize
// This can be either a connection url or you can pass the username, password, and host
const sequelize = new Sequelize("postgres://user:pass@example.com:5432/dbname");

//initialize one of our models for the database
class Activity extends Sequelize.Model {}
Activity.init(
  {
    //define the fields of our model
    activity_type: {
      type: Sequelize.STRING
    },
    duration: {
      type: Sequelize.INTEGER
    }
  },
  {
    //other options
    sequelize,
    modelName: "activity"
  }
);

// call sync to synchronize the table(s) with the model(s) defined above
// Also creates necessary tables if they don't already exist
sequelize
  .sync()
  .then(() => {
    //all is good
    logger.info("Synced to database successfully");
  })
  .catch(err => {
    //some error with syncing
    logger.error(err);
  });

//log each request using our bunyan logger before it gets routed
router.use((req, res, next) => {
  logger.info(req);
  next();
});

//API endpoint corresponding to a GET request at the base url
app.get("/", (req, res) => {
  //Use sequelize model to find all instances of an activity in the database
  //where the type of activity is running
  Activity.findAll({
    where: {
      activity_type: "running"
    }
  }).then(activities => {
    //log the result which should be an array of objects of type Activity
    //defined above in the Activity class
    logger.debug(activities);
  });
});

//Have our express app listen on the given port and log when it connects
app.listen(PORT, () => logger.info("App listening on port ${PORT}"));

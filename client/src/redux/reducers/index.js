import createReducer from "redux-starter-kit";
import { addActivity, removeActivity } from "../actions/index";

const rootReducer = createReducer([], {
  [addActivity.type]: (state, action) => {
    // "mutate" the array by calling push()
    state.push(action.payload);
  },
  [removeActivity.type]: (state, action) => {
    // Can still return an immutably-updated value if we want to
    return state.filter((activity, i) => i !== action.payload.index);
  }
});

export default rootReducer;

import createAction from "redux-starter-kit";
import axios from "axios";
import {
  ADD_ACTIVITY,
  REMOVE_ACTIVITY,
  FETCHED_ACTIVITIES
} from "action-types.js";

export const addActivity = createAction(ADD_ACTIVITY);
export const removeActivity = createAction(REMOVE_ACTIVITY);
export const fetchedActivities = createAction(FETCHED_ACTIVITIES);

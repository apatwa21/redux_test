import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchedActivities } from "../actions/index";
import axios from "axios";
class List extends Component {
  componentDidMount() {
    this.fetchActivities("some_url");
  }

  fetchActivities(url) {
    return dispatch => {
      axios
        .get(url)
        .then(res => res.json())
        .then(result => {
          dispatch(fetchedActivities(result));
        });
    };
  }

  render() {
    return (
      <ul>
        {this.props.activities.map(activity => (
          <li key={activity.id}>{activity.activity_type}</li>
        ))}
      </ul>
    );
  }
}
const mapStateToProps = state => {
  return {
    activities: state
  };
};

export default connect(mapStateToProps)(List);
